const filterTitlesContainer = document.getElementById('filter-titles');
const projects = document.querySelectorAll('.project-item');

filterTitlesContainer.addEventListener('click', (e) =>{
    if(e.target.classList.contains('work-tabs-title')) {
        const title = e.target;
        console.log(e.target);
        const type = title.dataset.filterby || 'project-item';
        const isActive = title.classList.contains('choosen');
        if(!isActive) {
            document.querySelector('.work-tabs-title.choosen').classList.remove('choosen');
            title.classList.add('choosen');
            filterbyClassName(projects, type);
        }
    }
})

function filterbyClassName(elements, className) {
    for(let element of elements) {
        element.hidden = !element.classList.contains(className);
    }
}