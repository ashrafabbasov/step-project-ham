let tabs = document.body.querySelector('ul');
let tabsTitle = document.body.getElementsByClassName('tabs-title');
let tabsContent = document.querySelectorAll('div.tabs-content');

console.log(tabs);
console.log(tabsTitle);
console.log(tabsContent);

tabs.addEventListener('click',function(event){
    event.target.classList.add('active');
    let i = 0;
    for(let elem of tabsTitle)
    {    
        if(elem!==event.target){
            elem.classList.remove('active');
            tabsContent[i].classList.add('hidden');
        }
        else{
            tabsContent[i].classList.remove('hidden');
        }
        i++;
    }
})